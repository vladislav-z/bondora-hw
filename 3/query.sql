select top (10) EnglishProductName, coalesce([22], 0.00) as "Week 22", coalesce([23], 0.00) as "Week 23", coalesce([24], 0.00) as "Week 24", coalesce([25], 0.00) as "Week 25", coalesce([26], 0.00) as "Week 26", coalesce([27], 0.00) as "Week 27", coalesce([28], 0.00) as "Week 28", coalesce([29], 0.00) as "Week 29"
from
	(select
		sum(SalesAmount) over(partition by EnglishProductName, WeekNumberOfYear) as WeeklySales,
		EnglishProductName,
		WeekNumberOfYear,
		TotalSalesIn2011
	from
		(select
			EnglishProductName,
			SalesAmount,
			datepart(wk, TallinnOrderDate) as WeekNumberOfYear,
			sum(SalesAmount) over(partition by EnglishProductName) as TotalSalesIn2011
		from 
			(select
				*,
				/* for SQL < 2016 */ convert(datetime, switchOffset(toDateTimeOffset(OrderDate, 0), '+02:00')) as TallinnOrderDate /* + timezonedb DST check */
				/* for SQL >= 2016 */ /* OrderDate at timezone "E. Europe Standard Time" as TallinnOrderDate */
			from dbo.FactInternetSales
			) as Sales
			left join dbo.DimCurrency on Sales.CurrencyKey = dbo.DimCurrency.CurrencyKey
			left join dbo.DimProduct on Sales.ProductKey = dbo.DimProduct.ProductKey
		where CurrencyAlternateKey = 'USD' and datepart(year, TallinnOrderDate) = 2011
		) as q
	where WeekNumberOfYear between 22 and 29
	) as q
pivot(min(WeeklySales) for WeekNumberOfYear in ([22], [23], [24], [25], [26], [27], [28], [29])) as pvt
order by TotalSalesIn2011 desc;