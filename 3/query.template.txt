select top (%N_PRODUCTS%) EnglishProductName, %COALESCED_WEEK_COLUMNS%
from
	(select
		sum(SalesAmount) over(partition by EnglishProductName, WeekNumberOfYear) as WeeklySales,
		EnglishProductName,
		WeekNumberOfYear,
		TotalSalesIn%YEAR%
	from
		(select
			EnglishProductName,
			SalesAmount,
			datepart(wk, TallinnOrderDate) as WeekNumberOfYear,
			sum(SalesAmount) over(partition by EnglishProductName) as TotalSalesIn%YEAR%
		from 
			(select
				*,
				/* for SQL < 2016 */ convert(datetime, switchOffset(toDateTimeOffset(OrderDate, 0), '+02:00')) as TallinnOrderDate /* + timezonedb DST check */
				/* for SQL >= 2016 */ /* OrderDate at timezone "E. Europe Standard Time" as TallinnOrderDate */
			from dbo.FactInternetSales
			) as Sales
			left join dbo.DimCurrency on Sales.CurrencyKey = dbo.DimCurrency.CurrencyKey
			left join dbo.DimProduct on Sales.ProductKey = dbo.DimProduct.ProductKey
		where CurrencyAlternateKey = '%CURRENCY_ALT_KEY%' and datepart(year, TallinnOrderDate) = %YEAR%
		) as q
	where WeekNumberOfYear between %WEEK_FROM% and %WEEK_TO%
	) as q
pivot(min(WeeklySales) for WeekNumberOfYear in (%WEEK_COLUMNS%)) as pvt
order by TotalSalesIn%YEAR% desc;