import sys

def generate_query(n_products, currency_alt_key, year, week_from, week_to, missing_value):
    
    ''' ASSUMES VALID AND SANITIZED INPUT! '''
    
    with open('./query.template.txt', 'r') as file:
        query = file.read()
        week_range = range(int(week_from), int(week_to) + 1)
        replacements = {
            'N_PRODUCTS': n_products,
            'YEAR': year,
            'WEEK_FROM': week_from,
            'WEEK_TO': week_to,
            'CURRENCY_ALT_KEY': currency_alt_key,
            'WEEK_COLUMNS': ', '.join(['[{}]'.format(w) for w in week_range])
        }
        replacements['COALESCED_WEEK_COLUMNS'] = ', '.join(['coalesce([{0}], {1}) as "Week {0}"'.format(w, missing_value) for w in week_range])
        for marker, repl in replacements.items():
            query = query.replace('%{}%'.format(marker), str(repl))
        return query

if __name__ == '__main__':
    if len(sys.argv) == 7:
        sys.stdout.write(generate_query(*sys.argv[1:]))
        sys.stdout.flush()