from sklearn import tree

class TreeHelper():
    
    def __init__(self, prefit_tree, feature_names):
        self._clf = prefit_tree
        self._feats = feature_names
        self._tree = prefit_tree.tree_
        self._parents = self._calc_parents()
    
    def _calc_parents(self):
        parents = [None] * self._tree.node_count
        for children_batch in [self._tree.children_left, self._tree.children_right]:
            for parent_id, child_id in enumerate(children_batch):
                if child_id != -1:
                    parents[child_id] = parent_id
        return parents
    
    def _get_node_target_rate(self, node_id):
        return self._tree.value[node_id][0][1] / self._tree.n_node_samples[node_id]
    
    def _get_matching_node_ids(self, min_node_size, min_target_rate):
        node_ids = []
        for i in range(self._tree.node_count):
            node_size = self._tree.n_node_samples[i]
            if node_size >= min_node_size and self._get_node_target_rate(i) >= min_target_rate:
                node_ids.append(i)
        return node_ids
    
    def _get_path_to_node(self, node_id):
        path = [node_id]
        current_node = node_id
        while current_node:
            current_node = self._parents[current_node]
            path.append(current_node)
        return path[::-1]
    
    def _get_edge_condition(self, node_src, node_dst):
        comp = ''
        if self._tree.children_left[node_src] == node_dst:
            comp = '<='
        if self._tree.children_right[node_src] == node_dst:
            comp = '>'
        return '"{}" {} {:.2f}'.format(self._feats[self._tree.feature[node_src]], comp, self._tree.threshold[node_src])
    
    def get_sql_rule(self, min_node_size, min_target_rate):
        disjuncts = []
        for i in self._get_matching_node_ids(min_node_size, min_target_rate):
            path = self._get_path_to_node(i)
            conjuncts = [self._get_edge_condition(a, b) for a, b in zip(path, path[1:])]
            disjuncts.append(' AND '.join(conjuncts))
        return ' OR '.join(disjuncts)
    
    def plot_highlight(self, min_node_size, min_target_rate):
        nodes = tree.plot_tree(self._clf, feature_names= self._feats)
        for i, node in enumerate(nodes):
            if i in self._get_matching_node_ids(min_node_size, min_target_rate):
                node.set_fontweight('bold')
                node.set_backgroundcolor((0.5, 0.75 + 0.25*self._get_node_target_rate(i), 0.5))
            else:
                node.set_backgroundcolor((1, 0.5 + 0.5*self._get_node_target_rate(i), 0.5))
        return nodes